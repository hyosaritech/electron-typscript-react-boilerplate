const path = require("path");
const rules = require("./webpack.rules");

function srcPath(src) {
  return path.join(__dirname, src);
}

module.exports = {
  mode: "development",
  devtool: "source-map",
  target: "electron-main",
  /**
   * This is the main entry point for your application, it's the first file
   * that runs in the main process.
   */
  entry: "./src/main/main.ts",
  // Put your normal webpack config below here
  module: {
    rules
  },
  resolve: {
    alias: {
      "@main": srcPath("src/main"),
      "@models": srcPath("src/models"),
      "@renderer": srcPath("src/renderer")
    },
    extensions: [".js", ".ts", ".jsx", ".tsx", ".css", ".json"]
  }
};
