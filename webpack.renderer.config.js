const rules = require("./webpack.rules");
const plugins = require("./webpack.plugins");
const path = require("path");

function srcPath(src) {
  return path.join(__dirname, src);
}

module.exports = {
  mode: "development",
  target: "electron-renderer",
  devtool: "source-map",
  module: {
    rules
  },
  plugins: plugins,
  resolve: {
    alias: {
      "@main": srcPath("src/main"),
      "@models": srcPath("src/models"),
      "@renderer": srcPath("src/renderer"),
      "react-dom": "@hot-loader/react-dom"
    },
    extensions: [".js", ".ts", ".jsx", ".tsx", ".css", ".json"]
  }
};
